// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let alg = ImageAsset(name: "alg")
  internal static let boss = ImageAsset(name: "boss")
  internal static let bot = ImageAsset(name: "bot")
  internal static let calendarWidget = ImageAsset(name: "calendarWidget")
  internal static let chess = ImageAsset(name: "chess")
  internal static let codeReview = ImageAsset(name: "codeReview")
  internal static let company1 = ImageAsset(name: "company1")
  internal static let company2 = ImageAsset(name: "company2")
  internal static let company3 = ImageAsset(name: "company3")
  internal static let company4 = ImageAsset(name: "company4")
  internal static let company5 = ImageAsset(name: "company5")
  internal static let computerGames = ImageAsset(name: "computerGames")
  internal static let docker = ImageAsset(name: "docker")
  internal static let eng = ImageAsset(name: "eng")
  internal static let english = ImageAsset(name: "english")
  internal static let games = ImageAsset(name: "games")
  internal static let hrChat = ImageAsset(name: "hrChat")
  internal static let linux = ImageAsset(name: "linux")
  internal static let loveYouBerryMuch = ImageAsset(name: "love-you-berry-much")
  internal static let man1 = ImageAsset(name: "man1")
  internal static let man2 = ImageAsset(name: "man2")
  internal static let man3 = ImageAsset(name: "man3")
  internal static let mentor = ImageAsset(name: "mentor")
  internal static let neitrino = ImageAsset(name: "neitrino")
  internal static let profile = ImageAsset(name: "profile")
  internal static let python = ImageAsset(name: "python")
  internal static let share = ImageAsset(name: "share")
  internal static let sport = ImageAsset(name: "sport")
  internal static let swift = ImageAsset(name: "swift")
  internal static let talks = ImageAsset(name: "talks")
  internal static let teamlead = ImageAsset(name: "teamlead")
  internal static let xcode = ImageAsset(name: "xcode")
  internal static let yoga = ImageAsset(name: "yoga")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
