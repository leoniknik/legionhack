//
//  AuthTabBar.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct AuthTabBar: View {
    
    var body: some View {
        TabView {
            NavigationView {
//                SurfaceView(mesh: <#Mesh#>, selection: <#SelectionHandler#>)
                DashboardView()
                    .navigationTitle("Дашборд").navigationBarTitleDisplayMode(.inline)
            }.tabItem {
                Image(systemName: "square.dashed.inset.filled")
                Text("Дашборд")
            }
            
            NavigationView {
                CommunicationView()
                    .navigationTitle("Коммуникации").navigationBarTitleDisplayMode(.inline)
            }.tabItem {
                Image(systemName: "message.fill")
                Text("Общение")
            }
            
            NavigationView {
                ProjectsRootView()
                    .navigationTitle("Проекты").navigationBarTitleDisplayMode(.inline)
            }.tabItem {
                Image(systemName: "hammer.fill")
                Text("Проекты")
            }
            
            NavigationView {
                ProfileOverviewView()
                    .navigationTitle("Профиль").navigationBarTitleDisplayMode(.inline)
            }.tabItem {
                Group {
                    Image(systemName: "person.crop.circle")
                    Text("Профиль")
                }
            }
        }
    }
}

struct AuthTabBar_Previews: PreviewProvider {
    static var previews: some View {
        AuthTabBar()
    }
}
