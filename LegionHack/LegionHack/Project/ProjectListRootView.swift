//
//  ProfileView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct ProjectsRootView: View {
    //var body: some View { ProjectDetailView(project: globProjects.first!) }
    //var body: some View { EmployeeHeaderView(isPersonalScreen: true, employee: empl4) }
    //var body: some View { OnboardingView(employee: empl4) }

    
    //
        var body: some View {
            VStack {
                ProjectsListView(projects: globProjects)
            }
            .navigationTitle(Text("Проекты"))
        }
}

struct ProjectsRootView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectsRootView()
    }
}


struct ProjectsListView: View {
    
    let projects: [ProjectModel]
    @State var selectedSmoothieID: Int = 0
    @State var searchString: String = ""
    @State private var favoriteColor = 0
    var searchSuggestions: [String] {
        return projects.compactMap { $0.name.contains(searchString) ? $0.name : nil }
    }
    
    var listedProjects: [ProjectModel] {
        guard favoriteColor == 1 else { return [globProjects.first!] }
        guard !searchString.isEmpty else { return projects }
        return projects
            .filter { $0.name.contains(searchString) }
            .sorted(by: { $0.name.localizedCompare($1.name) == .orderedAscending })
    }
    
    var body: some View {
        
        ScrollViewReader { proxy in
            List {
                Picker("What is your favorite color?", selection: $favoriteColor) {
                    Text("Мои проекты").tag(0)
                    Text("Проекты компании").tag(1)
                }
                .pickerStyle(.segmented)
                
                ForEach(listedProjects) { project in
                    NavigationLink() {
                        ProjectDetailView(project: project)
                    } label: {
                        ProjectView(projectModel: project)
                    }
                    .onChange(of: selectedSmoothieID) { newValue in
                        proxy.scrollTo(selectedSmoothieID)
                        selectedSmoothieID = selectedSmoothieID
                    }
                    .buttonStyle(PlainButtonStyle())
                    .swipeActions {
                        Button {
                        } label: {
                            Label {
                                Text("Следить зп проектом", comment: "Swipe action button in smoothie list")
                            } icon: {
                                Image(systemName: "bell.circle.fill")
                            }
                        }
                        .tint(.yellow)
                    }
                }
            }
            .searchable(text: $searchString) {
                ForEach(searchSuggestions, id: \.self) { suggestion in
                    Text(suggestion).searchCompletion(suggestion)
                }
            }
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct ProjectView: View {
    
    var projectModel: ProjectModel
    
    var body: some View {
        ZStack(alignment: .bottom) {
            Image(uiImage: projectModel.image)
                .resizable()
//                .aspectRatio(contentMode: .fill)
                .frame(height: 225)
                .accessibility(hidden: true)
            bottomBar
        }.clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
            .overlay {
                RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .strokeBorder(.quaternary, lineWidth: 0.5)
            }
    }
    
    var bottomBar: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(projectModel.name)
                    .font(.headline)
                    .bold()
                Text(projectModel.subname)
                    .foregroundStyle(.secondary)
                    .font(.subheadline)
            }
            
            Spacer()
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(.regularMaterial)
    }
}
