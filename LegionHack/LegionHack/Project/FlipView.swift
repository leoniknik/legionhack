//
//  FlipView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI
import MapKit

struct FlipMapView: View {
    var coodinate: CLLocationCoordinate2D
    var presenting: Bool = true
    var closeAction: () -> Void = {}
    
    @State private var visibleSide = true
    
    var body: some View {
        FlipView(visibleSide: visibleSide) {
            LegionMapView(coordinate: coodinate, closeAction: {},flipAction: flipCard)
        } back: {
            VStack{
                Text(LongTextStore.place)
                Button(action: flipCard){
                    Text("Назад")
                        .foregroundColor(.white)
                }
                    .frame(width: 100, height: 30)
                    .background(legionBlack)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    
            }
        }
        .contentShape(Rectangle())
        .animation(.flipCard, value: visibleSide)
    }
    
    func flipCard() {
        visibleSide.toggle()
    }
    
}

struct FlipView<Front: View, Back: View>: View {
    var visibleSide: Bool
    @ViewBuilder var front: Front
    @ViewBuilder var back: Back

    var body: some View {
        ZStack {
            front
                .modifier(FlipModifier(side: true, visibleSide: visibleSide))
            back
                .modifier(FlipModifier(side: false, visibleSide: visibleSide))
        }
    }
}

struct FlipModifier: AnimatableModifier {
    var side: Bool
    var flipProgress: Double
    
    init(side: Bool, visibleSide: Bool) {
        self.side = side
        self.flipProgress = visibleSide == true ? 0 : 1
    }
    
    public var animatableData: Double {
        get { flipProgress }
        set { flipProgress = newValue }
    }
    
    var visible: Bool {
        switch side {
        case true:
            return flipProgress <= 0.5
        case false:
            return flipProgress > 0.5
        }
    }

    public func body(content: Content) -> some View {
        ZStack {
            content
                .opacity(visible ? 1 : 0)
                .accessibility(hidden: !visible)
        }
        .scaleEffect(x: scale, y: 1.0)
        .rotation3DEffect(.degrees(flipProgress * -180), axis: (x: 0.0, y: 1.0, z: 0.0), perspective: 0.5)
    }

    var scale: CGFloat {
        switch side {
        case true:
            return 1.0
        case false:
            return -1.0
        }
    }
}

extension Animation {
    static let flipCard = Animation.spring(response: 0.35, dampingFraction: 0.7)
}

struct LegionMapView: View {
    init(coordinate: CLLocationCoordinate2D,  closeAction: @escaping () -> (), flipAction: @escaping () -> ()) {
        self.coordinate = coordinate
        region = MKCoordinateRegion(
            center: coordinate,
            span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        )
        self.flipAction = flipAction
        self.closeAction = closeAction
    }
    
    var coordinate: CLLocationCoordinate2D
    var closeAction: () -> ()
    var flipAction: () -> ()
    @State var region: MKCoordinateRegion

    var body: some View {
        ZStack(alignment: .topTrailing) {
            Map(coordinateRegion: $region)
            Image(systemName: "info.circle")
                .resizable()
                .frame(width: 30, height: 30)
                .padding()
                
        }
        
    }
}

struct LegionMapView_Previews: PreviewProvider {
    static var previews: some View {
        LegionMapView(coordinate: CLLocationCoordinate2D(latitude: 34.011_286, longitude: -116.166_868), closeAction: {}, flipAction: {})
    }
}
