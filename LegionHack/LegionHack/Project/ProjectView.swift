//
//  ProjectView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 01.10.2021.
//

import Foundation
import SwiftUI
import MapKit


struct DescriptionView: View {
    var descriptionModel: DescriptionModel
    var aligment: HorizontalAlignment
    
    var body: some View {
        VStack(alignment: aligment) {
            Text(descriptionModel.label1)
                .font(.title)
            Text(descriptionModel.label2)
                .font(.title2)
        }
    }
}

struct ProjectDetailView: View {
    var project: ProjectModel
    
    var body: some View {
        List {
            Image(uiImage: project.image)
                .resizable()
                .ignoresSafeArea(edges: .top)
                .frame(height: 200)
            DescriptionView(descriptionModel: project.descriptionModel, aligment: .leading)
            EmployeesView(employeesViewModel: project.leaders)
            EmployeesView(employeesViewModel: project.legioners)
            TechnologiesView(technologies: project.technologies, title: "Технологии проекта", hideComparation: true)
            FlipMapView(coodinate: CLLocationCoordinate2D(latitude: 43.681577, longitude: 40.205757))
                .frame(height: 300)
        }
        .navigationTitle(Text("О проекте"))
    }
}

struct ProjectDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectDetailView(project: globProjects.first!)
    }
}

struct EmployeesViewModel {
    var title: String
    var employees: [Employee]
    
    static var stub: EmployeesViewModel {
        return EmployeesViewModel(
            title: "stub",
            employees: [
                Employee.stub,
                Employee.stub,
                Employee.stub
            ])
    }
}

struct EmployeesView: View {
    var employeesViewModel: EmployeesViewModel?
    var hideIco: Bool = false
    //var background: Color = .regularMaterial
    
    var body: some View {
        if let employeesViewModel = employeesViewModel {
            HStack{
                VStack(alignment: .leading) {
                    VStack(alignment: .leading, spacing: 10) {
                        if !hideIco {
                            Image(systemName: "dot.arrowtriangles.up.right.down.left.circle")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .clipShape(Circle())
                        }
                        Text(employeesViewModel.title)
                            .font(.headline)
                    }
                    
                    ForEach(employeesViewModel.employees) { employee in
                        EmployeeView(employee: employee)
                    }
                }
                Spacer()
            }
            .padding(20)
            //.frame(maxWidth: .infinity)
            .background(RoundedRectangle(cornerSize: CGSize(width: 10,height: 10)).fill(legionLightBlue))
        } else {
            
        }
    }
}


struct RestView: View {
    @State var progress: Float
    @State var color: Color
    //var background: Material = .regularMaterial
    var days: Int
    var body: some View {
            VStack(alignment: .leading) {
                    Text("Время")
                        .font(.headline)
                HStack{
                    ImageProgressBar(progress: $progress, color: color, image: Image(systemName: "clock.badge.exclamationmark"))
                        .frame(width: 20, height: 20)
                    Text("\(days) дней")
                        .font(.headline)
                }
            }
            .padding(20)
            //.frame(maxWidth: .infinity)
            .background(RoundedRectangle(cornerSize: CGSize(width: 10,height: 10)).fill(legionLightBlue))
    }
}

struct EmployeeView: View {
    var employee: Employee
    var body: some View {
        HStack {
            Image(uiImage: employee.image)
                .resizable()
                .frame(width: 40, height: 40, alignment: .center)
                .clipShape(Circle())
            VStack(alignment: .leading) {
                Text(employee.name)
                    .font(.headline)
                Text(employee.surname)
                    .foregroundStyle(.secondary)
            }
        }
    }
}
