//
//  LongTextStore.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation

struct LongTextStore {
    static func docsText(helperPerson: Employee?) -> String { return  """
    Тебе нужно сходить к \(helperPerson?.surname ?? "Stubname") \(helperPerson?.name ?? "Stubname") и затем делать следующее.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    """
    }
    
    static func docs2Text(helperPerson: Employee?) -> String { return  """
    Тебе нужно сходить к \(helperPerson?.surname ?? "Stubname") \(helperPerson?.name ?? "Stubname") и затем делать следующее.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo
    """
    }
    
    static var place: String = """
    Компания базируется на холмах Сочи в живописном месте.
    По выходным можно кататься на лыжах. Дождей почти не бывает.
    Средняя температура +10 круглый год. Траспортная доступность хорошая
    """
}

