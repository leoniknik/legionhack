//
//  TechnologyView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

struct TechnologyView: View {
    var technology: KnownTech
    
    var color: Color {
        if let isKnown = technology.isKnown {
            return isKnown ? .green : .orange
        } else {
            return .white
        }
    }
    
    var body: some View {
        VStack(alignment: .center, spacing: 2) {
            Image(uiImage: technology.technology.image)
                .resizable()
                .frame(width: 20, height: 20)
            Text(technology.technology.name)
            //                .font(.title3.bold())
        }
        .padding(5)
        .background(color)
        .clipShape(RoundedRectangle(cornerRadius: 6))
        
    }
}


struct KnownTech: Identifiable {
    var id = UUID()
    var technology: Technology
    var isKnown: Bool?
}
struct TechnologiesView: View {
    var technologies: [Technology]
    var myTech = empl4.technoligies
    var title: String
    
    var hideComparation: Bool
    
    @State var comparing: Bool = false
    
    var knownTech: [KnownTech] {
        if comparing {
            var known = [Technology]()
            var unknown = [Technology]()
            for tech in technologies {
                if myTech.first(where: { $0.name == tech.name }) != nil {
                    known.append(tech)
                } else {
                    unknown.append(tech)
                }
            }
            return known.map { KnownTech(technology: $0, isKnown: true) }
            + unknown.map { KnownTech(technology: $0, isKnown: false) }
        } else {
            return technologies.map { KnownTech(technology: $0, isKnown: nil) }
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text(title)
                .font(.title3)
                .foregroundColor(.white)
                .padding(EdgeInsets(top: 16, leading: 23, bottom: 16, trailing: 10))
            ScrollView (.horizontal, showsIndicators: false) {
                HStack {
                    Rectangle()
                        .fill(.clear)
                        .frame(width: 16, height: 1)
                    ForEach(knownTech) { tech in
                        TechnologyView(technology: tech)
                    }
                    Rectangle()
                        .fill(.clear)
                        .frame(width: 16, height: 1)
                        
                }
            }.frame(height: 40)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0))
            
            if hideComparation {
                HStack {
                    Spacer()
                    //                NavigationLink(destination: Text("")) {
                    ZStack {
                        RoundedRectangle(cornerRadius: 10)
                            .fill(.regularMaterial)
                            .frame(width: 300, height: 50)
                        Text("Сравнить мои навыки с требованиями проекта")
                            .font(.title3)
                            .foregroundColor(legionBlack)
                        
                    }
                    .padding(10)
                    .gesture(TapGesture().onEnded({ comparing.toggle() }))
                    //                }
                    Spacer()
                }.navigationBarTitle("Сранение")
            }
        }
        .background(legionBlack)
    }
}

struct TechnologiesView_Previews: PreviewProvider {
    static var previews: some View {
        TechnologiesView(technologies: globProjects.first!.technologies, title: "Title", hideComparation: true)
    }
}
