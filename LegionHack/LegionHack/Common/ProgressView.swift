//
//  ProgressView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

//struct ExampleOfProgress: View {
//  @State var value = 0
//  var maximum = 10
//  var body: some View {
//    VStack(alignment: .leading) {
//      Text("SegmentedProgressView example")
//        .font(.headline)
//      Text("Current value is \(value) out of \(maximum)")
//        .font(.body)
//      SegmentedProgressView(value: value, maximum: maximum)
//        .padding(.vertical)
//      Button(action: {
//        self.value = (self.value + 1) % (self.maximum + 1)
//      }) {
//        Text("Increment value")
//      }
//    }
//    .padding()
//  }
//}


struct SegmentedProgressView: View {
  var value: Int
  var maximum: Int = 7
  var height: CGFloat = 20
  var spacing: CGFloat = 6
  var selectedColor: Color = .accentColor
  var unselectedColor: Color = Color.secondary.opacity(0.3)
  var body: some View {
    HStack(spacing: spacing) {
      ForEach(0 ..< maximum) { index in
        Rectangle()
          .foregroundColor(index < self.value ? self.selectedColor : self.unselectedColor)
      }
    }
    .frame(maxHeight: height)
    .clipShape(Capsule())
  }
}



struct ProgressBar: View {
    @Binding var progress: Float
    var color: Color
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 20.0)
                .opacity(0.3)
                .foregroundColor(color)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 20.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(color)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)

            Text(String(format: "%.0f %%", min(self.progress, 1.0)*100.0))
                .font(.title2)
                .bold()
        }
    }
}

struct ImageProgressBar: View {
    @Binding var progress: Float
    var color: Color
    var image: Image
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 2.0)
                .opacity(0.3)
                .foregroundColor(color)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 2.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(color)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)

            image
        }
    }
}

