//
//  State.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI


var empl1 = Employee(name: "Александр", surname: "Евтухов", image: Asset.man1.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl2 = Employee(name: "Игорь", surname: "Котик", image: Asset.man2.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl3 = Employee(name: "Евгений", surname: "Соловьев", image: Asset.man3.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl5 = Employee(name: "Полина", surname: "Орлова", image: Asset.man1.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl6 = Employee(name: "Алена", surname: "Синичкина", image: Asset.man1.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl7 = Employee(name: "Александра", surname: "Евтухова", image: Asset.man1.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])
var empl8 = Employee(name: "Нина", surname: "Эррера", image: Asset.man1.image, grade: .low, technoligies: [Technology.stub], myBest: [MyBestViewModel.stub])



var empl4 = Employee(
    name: "Роман",
    surname: "iOS Developer",
    image: Asset.profile.image,
    grade: .low,
    technoligies: [
        swiftTech,
        microTech,
        amplTech
    ],
    myBest: [
        MyBestViewModel(title: "Я пишу кода больше чем#сотрудников моей компании", value: 0.68, color: .green),
        MyBestViewModel(title: "#своих фичей я делаю не срывая сроков", value: 0.80, color: .blue),
        MyBestViewModel(title: "Всего#моих багов находят на регрессе", value: 0.01, color: .red),
        MyBestViewModel(title: "Я принимаю участие в#командных мероприятий", value: 1, color: .purple)
    ],
    mentor: empl1,
    supervisor: empl2,
    hrPartner: empl3,
    onboardingSteps: [
        OnboardingStepViewModel(
            title: "Оформление документов",
            todo: LongTextStore.docsText(helperPerson: empl1),
            helperPerson: empl1,
            substeps: [
                OnboardingItemViewModel(id: 1, completed: true, todo: "Получить трудовую"),
                OnboardingItemViewModel(id: 2, completed: false, todo: "Получить конспект по безопаасности"),
                OnboardingItemViewModel(id: 3, completed: false, todo: "Получить ключ карту")
            ]
        ),
        
        OnboardingStepViewModel(
            title: "Получение техники",
            todo: LongTextStore.docs2Text(helperPerson: empl1),
            helperPerson: empl2,
            substeps: [
                OnboardingItemViewModel(id: 1, completed: true, todo: "Заполнить заказ"),
                OnboardingItemViewModel(id: 2, completed: false, todo: "Получить технику"),
                OnboardingItemViewModel(id: 3, completed: false, todo: "Заполнить техничесчкую карту")
            ]
        ),
        
        OnboardingStepViewModel(
            title: "Знакомство с командой",
            todo: LongTextStore.docsText(helperPerson: empl1),
            helperPerson: empl3,
            substeps: [
                OnboardingItemViewModel(id: 1, completed: true, todo: "Подготовить самопрезентацию"),
                OnboardingItemViewModel(id: 2, completed: false, todo: "Рассказать о себе"),
            ]
        ),
        
        OnboardingStepViewModel(
            title: "Изучение документов",
            todo: LongTextStore.docsText(helperPerson: empl1),
            helperPerson: empl1,
            substeps: [
                OnboardingItemViewModel(id: 1, completed: true, todo: "Изучить архитектуру проекта"),
                OnboardingItemViewModel(id: 2, completed: false, todo: "Изучить code-style"),
                OnboardingItemViewModel(id: 3, completed: false, todo: "Изучить процесс релиза")
            ]
        ),
        
        OnboardingStepViewModel(
            title: "Написание фичи",
            todo: LongTextStore.docsText(helperPerson: empl1),
            helperPerson: empl2,
            substeps: [
                OnboardingItemViewModel(id: 1, completed: true, todo: "Взять задачу"),
                OnboardingItemViewModel(id: 2, completed: false, todo: "Изучить аналитику"),
                OnboardingItemViewModel(id: 3, completed: false, todo: "Написать код"),
                OnboardingItemViewModel(id: 4, completed: false, todo: "Отправить на тестирование")
            ]
        ),
    ]
)

var swiftTech = Technology(image: UIImage(systemName: "swift")!, name: "Swift")
var microTech = Technology(image: UIImage(systemName: "point.3.filled.connected.trianglepath.dotted")!, name: "Микросервисы")
var amplTech = Technology(image: UIImage(systemName: "chart.line.uptrend.xyaxis.circle.fill")!, name: "Amplitude")
var swifUI = Technology(image: UIImage(systemName: "text.insert")!, name: "SwiftUI")
var concurency = Technology(image: UIImage(systemName: "alt")!, name: "Многопоточность")

var globProjects = [
    ProjectModel(
        image: Asset.company3.image,
        name: "LoremIpsum",
        subname: "Построй свое будущее",
        descriptionModel: DescriptionModel(
            label1: "Компания номер 1 в Росии",
            label2: "Строим дома для всех"
        ),
        leaders: EmployeesViewModel(
            title: "Руководители проекта",
            employees: [
                empl1,
                empl2,
                empl3
            ]
        ),
        legioners: EmployeesViewModel(
            title: "Легионеры",
            employees: [
                empl4,
                empl5,
                empl6,
                empl7,
                empl8
            ]
        ),
        coordinate1: 12,
        coordinate2: 12,
        technologies: [
            swiftTech,
            swifUI,
            microTech,
            amplTech,
            concurency
        ]
    ),
    
    ProjectModel(
        image: Asset.company4.image,
        name: "Tencend Game",
        subname: "Погрузись в удивительные миры",
        descriptionModel: DescriptionModel(
            label1: "Top 3 в мире гейминга",
            label2: "Реализуй свои мечты"
        ),
        leaders: EmployeesViewModel(
            title: "Руководители проекта",
            employees: [
                empl1,
                empl2,
                empl3
            ]
        ),
        legioners: EmployeesViewModel(
            title: "Легионеры",
            employees: [
                empl4,
                empl5,
                empl6,
                empl7,
                empl8
            ]
        ),
        coordinate1: 12,
        coordinate2: 12,
        technologies: [
            swiftTech,
            microTech,
            amplTech,
            swifUI,
            concurency
        ]
    ),
    ProjectModel(
        image: Asset.company5.image,
        name: "GreenFood",
        subname: "Вкусное здоровое питание",
        descriptionModel: DescriptionModel(
            label1: "ПП для каждого",
            label2: "Всегда свежие овощи и фрукты"
        ),
        leaders: EmployeesViewModel(
            title: "Руководители проекта",
            employees: [
                empl1,
                empl2,
                empl3
            ]
        ),
        legioners: EmployeesViewModel(
            title: "Легионеры",
            employees: [
                empl4,
                empl5,
                empl6,
                empl7,
                empl8
            ]
        ),
        coordinate1: 12,
        coordinate2: 12,
        technologies: [
            swiftTech,
            swifUI,
            microTech,
            amplTech,
            concurency
        ]
    ),
    ProjectModel(
        image: Asset.company1.image,
        name: "Amazon",
        subname: "Доставим всем",
        descriptionModel: DescriptionModel(
            label1: "Привезем подарок вовремя",
            label2: "Доставим в любой уголок земного шара"
        ),
        leaders: EmployeesViewModel(
            title: "Руководители проекта",
            employees: [
                empl1,
                empl2,
                empl3
            ]
        ),
        legioners: EmployeesViewModel(
            title: "Легионеры",
            employees: [
                empl4,
                empl5,
                empl6,
                empl7,
                empl8
            ]
        ),
        coordinate1: 12,
        coordinate2: 12,
        technologies: [
            swiftTech,
            swifUI,
            microTech,
            amplTech,
            
            concurency
        ]
    ),
    ProjectModel.stub(img: Asset.company2.image),
    ProjectModel.stub(img: Asset.company1.image),
    ProjectModel.stub(img: Asset.company4.image),
    ProjectModel.stub(img: Asset.company5.image)
]

var quests: [SingleQuestViewModel] = [
    SingleQuestViewModel(
        star: 2,
        type: "Доклад",
        title: "Подготовить доклад на тему DevSecOps",
        price: 25,
        todo: "Нужно сделать подробный доклад о базовых практиках DevSecOps и выступить с ним перед командой",
        empl: empl1,
        progress: 0.5,
        color: .red,
        days: 5
    ),
    SingleQuestViewModel(
        star: 2,
        type: "Саморазвитие",
        title: "Изучить новую технологию iOs",
        price: 25,
        todo: "Нужно сделать подробный доклад об изученной технологии с описанием ее особенностей",
        empl: empl3,
        progress: 0.5,
        color: .red,
        days: 5
    )
]

var quests2 = [
    SingleQuestViewModel(
        star: 2,
        type: "Собеседование",
        title: "Подготовить вопросы для проведения технического собеседования",
        price: 25,
        todo: "Нужно сделать файл с 10 вопросами для проведения такого собеседования",
        empl: empl4,
        progress: 0.5,
        color: .red,
        days: 5
    ),
    SingleQuestViewModel(
        star: 2,
        type: "Саморазвитие",
        title: "Изучить новую технологию Android",
        price: 25,
        todo: "Нужно сделать подробный доклад об изученной технологии с описанием ее особеннойстей",
        empl: empl1,
        progress: 0.5,
        color: .red,
        days: 5
    ),
    SingleQuestViewModel(
        star: 2,
        type: "Собеседование",
        title: "Подготовить вопросы для проведения собеседования",
        price: 25,
        todo: "Нужно сделать подробный файл с вопросами и компетенциями для проведения Sjft skills собеседования",
        empl: empl5,
        progress: 0.5,
        color: .red,
        days: 5
    ),
    SingleQuestViewModel(
        star: 5,
        type: "Ментроство",
        title: "Менторство над стажером",
        price: 200,
        todo: "Нужно помогать стажеру влиться в проект. Отвечать на его вопрос и помогать изучать новое",
        empl: empl2,
        progress: 0.8,
        color: .green,
        days: 25
    )
]
