//
//  OnboardingView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

class OnboardingStepViewModel: ObservableObject {
    internal init(
        title: String,
        todo: String,
        helperPerson: Employee? = nil,
        substeps: [OnboardingItemViewModel],
        currentSubstep: Int = 0
    ) {
        self.title = title
        self.todo = todo
        self.helperPerson = helperPerson
        self.substeps = substeps
        self.currentSubstep = currentSubstep
    }
    
    var title: String
    var todo: String
    weak var helperPerson: Employee?
    var substeps: [OnboardingItemViewModel]
    @Published var currentSubstep: Int = 0
    
    static var stub: OnboardingStepViewModel {
        OnboardingStepViewModel(
            title: "Stub",
            todo: "Stub",
            helperPerson: nil,
            substeps: [
                OnboardingItemViewModel.stub,
                OnboardingItemViewModel.stub
            ]
        )
    }
}

struct OnboardingStepView: View {
    var step: OnboardingStepViewModel
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("Что тебе нужно сделать")
                    .font(.title)
                Text(step.todo)
                    .font(.system(size: 10))
                    .foregroundColor(.secondary)
                    .lineLimit(nil)
            }.padding(10)
        }
        .background(.regularMaterial)
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .padding(5)
    }
}

class OnboardingItemViewModel: Identifiable, ObservableObject {
    internal init(id: Int,completed: Bool, todo: String) {
        self.id = id
        self.completed = completed
        self.todo = todo
    }
    
    var id: Int
    @Published var completed: Bool
    var todo: String
    
    static var stub: OnboardingItemViewModel {
        return OnboardingItemViewModel(id: -1, completed: false, todo: "Stub")
    }
}

struct OnboardingItemView: View {
    @ObservedObject var step: OnboardingItemViewModel
    var body: some View {
        HStack
        {
            let color: Color = step.completed ? .green : .white
            let borderColor: Color = step.completed ? .green : .gray
            ZStack {
                Circle().fill(borderColor)
                    .frame(width: 36, height: 36)
                
                Circle().fill(color)
                    .frame(width: 30, height: 30)
                Text("\(step.id)")
            }
            
            Text(step.todo)
                .font(.title3)
                .foregroundColor(.secondary)
        }
    }
}


struct OnboardingView: View {
    @ObservedObject var employee: Employee
    @Binding var isShowing: Bool
    var step: OnboardingStepViewModel {
        employee.onboardingSteps[employee.onboardingStep]
    }
    
    var incrementerStepCounter: Int {
        employee.onboardingStep + 1
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text("Прогресс онбординга")
                    .font(.title)
                SegmentedProgressView(value: incrementerStepCounter, maximum: employee.onboardingSteps.count)
                Divider()
                Text("Фаза \(incrementerStepCounter)")
                    .font(.title)
                Text(step.title)
                    .font(.title2)
                    .foregroundColor(.secondary)
                Group {
                    OnboardingStepView(step: step)
                    ForEach(step.substeps) { st in
                        OnboardingItemView(step: st)
                    }
                }
                if let helper = step.helperPerson {
                    EmployeesView(employeesViewModel: EmployeesViewModel(title: "Кто тебе поможет с данным шагом", employees: [helper]), hideIco: true)
                }
                
                Button(action: { }) {
                    HStack {
                        Spacer()
                        Text("Обраиться в чат")
                            .foregroundColor(.white)
                        Spacer()
                    }
                   }
                    .frame(height: 40)
                    .background(Color.blue)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .padding(EdgeInsets(top: 5, leading: 20, bottom: 0, trailing: 20))
                
                Button(action: {
//                    step.substeps
                    if step.currentSubstep + 1 < step.substeps.count {
                        step.currentSubstep += 1
                        step.substeps[step.currentSubstep].completed = true
                    } else {
                        if employee.onboardingStep + 1 < employee.onboardingSteps.count {
                            employee.onboardingStep += 1
                        } else {
                            isShowing = false
                            empl4.isOnboardingAvailable = false
                        }
                    }
                    
                }) {
                    HStack {
                        Spacer()
                        Text("На следующий шаг")
                            .foregroundColor(.white)
                        Spacer()
                    }
                   }
                    .frame(height: 40)
                    .background(Color.blue)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .padding(EdgeInsets(top: 5, leading: 20, bottom: 0, trailing: 20))
            }
        }
        .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
    }
}
