//
//  EmployeeHeaderView.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

struct EmployeeHeaderView: View {
    var isPersonalScreen: Bool
    
    var employee: Employee
    var descriptionModel: DescriptionModel {
        DescriptionModel(label1: employee.surname, label2: employee.name)
    }
    
    var hrEmployeesViewModel: EmployeesViewModel? {
        guard  let hr = employee.hrPartner else { return nil }
        return EmployeesViewModel(title: "HR партнер", employees: [hr])
    }
    
    var leadEmployeesViewModel: EmployeesViewModel? {
        guard  let hr = employee.supervisor else { return nil }
        return EmployeesViewModel(title: "Руководитель", employees: [hr])
    }
    
    var mentorEmployeesViewModel: EmployeesViewModel? {
        guard  let hr = employee.mentor else { return nil }
        return EmployeesViewModel(title: "Ментор", employees: [hr])
    }
    
    var body: some View {
        VStack {
            Image(uiImage: Asset.profile.image)
                .resizable()
                .ignoresSafeArea(edges: .top)
                .frame(width: 100, height: 100)
                .cornerRadius(50)
            DescriptionView(descriptionModel: descriptionModel, aligment: .center)
            if !isPersonalScreen {
                EmployeesView(employeesViewModel: hrEmployeesViewModel, hideIco: true)
                .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                EmployeesView(employeesViewModel: leadEmployeesViewModel, hideIco: true)
                .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                EmployeesView(employeesViewModel: mentorEmployeesViewModel, hideIco: true)
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
            }
            
            TechnologiesView(technologies: employee.technoligies, title: "Мои технологии", hideComparation: false)
        }
    }
}

struct EmployeeHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            EmployeeHeaderView(isPersonalScreen: true, employee: empl4)
//                .frame(width: 180, height: 180)
            .previewDisplayName("Thumbnail")
            
            EmployeeHeaderView(isPersonalScreen: false, employee: empl4)
//                .frame(width: 180, height: 180)
            .previewDisplayName("Thumbnail2")
            
        }.previewLayout(.sizeThatFits)
        
    }
}
