//
//  NavigationLazyView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct NavigationLazyView<Content: View>: View {
    let build: () -> Content
    
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    
    var body: Content {
        build()
    }
}
