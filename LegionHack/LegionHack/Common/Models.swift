//
//  Models.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

struct ProjectModel:Identifiable {
    let id = UUID()
    var image: UIImage
    var name: String
    var subname: String
    
    var descriptionModel : DescriptionModel
    var leaders: EmployeesViewModel
    var legioners: EmployeesViewModel
    
    var coordinate1: Double
    var coordinate2: Double
    var technologies: [Technology]
    
    static func stub(img: UIImage) -> ProjectModel {
        return ProjectModel(
            image: img,
            name: "Stub",
            subname: "Stub",
            descriptionModel: DescriptionModel.stub,
            leaders: EmployeesViewModel.stub,
            legioners: EmployeesViewModel.stub,
            coordinate1: 12,
            coordinate2: 12,
            technologies: [
                Technology.stub,
                Technology.stub
            ]
        )
    }
}

struct DescriptionModel {
    var label1: String
    var label2: String
    
    static var stub: DescriptionModel {
        return DescriptionModel(label1: "label1", label2: "label2")
    }
}

struct Technology: Identifiable {
    var id = UUID()
    var image: UIImage
    var name: String
    
    static var stub: Technology {
        return Technology(image: UIImage(systemName: "asterisk.circle.fill")!, name: "Stub")
    }
}

class Employee: Identifiable, ObservableObject {
    internal init(
        id: UUID = UUID(),
        name: String,
        surname: String,
        image: UIImage,
        grade: Grade,
        technoligies: [Technology],
        myBest: [MyBestViewModel],
        mentor: Employee? = nil,
        supervisor: Employee? = nil,
        hrPartner: Employee? = nil,
        onboardingSteps: [OnboardingStepViewModel] = [ OnboardingStepViewModel.stub,  OnboardingStepViewModel.stub ],
        onboardingStep: Int = 0
    ) {
        self.id = id
        self.name = name
        self.surname = surname
        self.image = image
        self.grade = grade
        self.mentor = mentor
        self.myBest = myBest
        self.supervisor = supervisor
        self.hrPartner = hrPartner
        self.technoligies = technoligies
        self.onboardingSteps = onboardingSteps
        self.onboardingStep = onboardingStep
    }
    
    enum Grade {
        case low
        case medium
        case high
    }
    
    var id = UUID()
    var name: String
    var surname: String
    var image: UIImage
    var technoligies: [Technology]
    var myBest: [MyBestViewModel]
    var onboardingSteps: [OnboardingStepViewModel]
    @Published var onboardingStep: Int
    @Published var isOnboardingAvailable: Bool = true
    
    var grade: Grade
    weak var mentor: Employee?
    weak var supervisor: Employee?
    weak var hrPartner: Employee?
    
    
    static var stub: Employee {
        return Employee(name: "Stub", surname: "Stub", image: UIImage(), grade: .low, technoligies: [Technology.stub,Technology.stub], myBest: [MyBestViewModel.stub])
    }
}
