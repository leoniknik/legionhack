//
//  OneOnOneView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct OneOnOneWidgetView: View {
    let viewModel: OneOnOneViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            Image("teamlead")
                .resizable()
                .frame(width: 200, height: 200)
                .cornerRadius(8)
            Text("Обратная связь (One on One) c функциональным руководителем")
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .font(.system(size: 18, weight: .bold))
        }.frame(width: 350, height: 350).background(legionBlack).cornerRadius(12)
    }
}
