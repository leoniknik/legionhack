//
//  OnboardingView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct OnboardingWidgetView: View {
    let viewModel: OnboardingViewModel
    
    var h: CGFloat {
        employee.isOnboardingAvailable ? 350 : 0
    }
    
    @ObservedObject var employee: Employee
    
    var step: OnboardingStepViewModel {
        employee.onboardingSteps[employee.onboardingStep]
    }
    
    var incrementerStepCounter: Int {
        employee.onboardingStep + 1
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Прогресс онбординга")
                .font(.title)
                .foregroundColor(legionBlack)
            Text(step.title)
                .foregroundColor(.secondary)
            SegmentedProgressView(value: incrementerStepCounter, maximum: employee.onboardingSteps.count)
            VStack(alignment: .leading, spacing: 12) {
                ForEach(step.substeps) { substep in
                    OnboardingItemView(step: substep)
                }
            }
        }
        .padding()
        .frame(width: 350, height: h)
        .background(.regularMaterial)
        .cornerRadius(12)
    }
}
