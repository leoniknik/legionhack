//
//  TaskTrackerView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct TaskTrackerWidgetView: View {
    let viewModel: TaskTrackerViewModel
    
    var body: some View {
        List {
            Section(header: Text("Issues assigned to me")) {
                Text("Завершить настройку CI машины")
                Text("Bugprod 781")
            }
            Section(header: Text("Reported issues")) {
                Text("Подарки для новых сотрудников")
                Text("Презентация хакатона")
                Text("Дайджест")
            }
        }.frame(width: 350, height: 350).cornerRadius(12)
    }
}
