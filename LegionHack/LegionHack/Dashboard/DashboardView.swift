//
//  DashboardView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct CalendarViewModel {
    
}

struct OnboardingViewModel {
    let employee: Employee
}

struct CodeReviewViewModel {
    
}

struct TaskTrackerViewModel {
    
}

struct OneOnOneViewModel {
    
}

struct QuestViewModel {
    
}

struct DashboardViewModel: Identifiable {
    enum ViewType {
        case calendar(CalendarViewModel)
        case onboarding(OnboardingViewModel)
        case codeReview(CodeReviewViewModel)
        case tastTracker(TaskTrackerViewModel)
        case oneOnOne(OneOnOneViewModel)
        case quest(QuestViewModel)
    }
    
    let id = UUID()
    let type: ViewType
}

class DashboardViewViewModel: ObservableObject {
    @Published var boards: [DashboardViewModel] = [
        .init(type: .calendar(.init())),
        .init(type: .onboarding(.init(employee: empl4))),
        .init(type: .codeReview(.init())),
        .init(type: .tastTracker(.init())),
        .init(type: .oneOnOne(.init())),
    ]
}

struct DashboardView: View {
    @StateObject var viewModel = DashboardViewViewModel()
    @State var isOnboardingShown: Bool = false
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: [GridItem(.flexible())]) {
                ForEach(viewModel.boards) { viewModel in
                    switch viewModel.type {
                    case .calendar(let calendarViewModel):
                        CalendarWidgetView(viewModel: calendarViewModel)
                    case .onboarding(let onboardingViewModel):
                        if empl4.isOnboardingAvailable {
                            OnboardingWidgetView(viewModel: onboardingViewModel, employee: empl4)
                                .onTapGesture {
                                    isOnboardingShown = true
                                }
                        }
                    case .codeReview(let codeReviewViewModel):
                        CodeReviewWidgetView(viewModel: codeReviewViewModel)
                    case .tastTracker(let taskTrackerViewModel):
                        TaskTrackerWidgetView(viewModel: taskTrackerViewModel)
                    case .oneOnOne(let oneOnOneViewModel):
                        OneOnOneWidgetView(viewModel: oneOnOneViewModel)
                    case .quest(let questViewModel):
                        QuestWidgetView(viewModel: questViewModel)
                    }
                }
                
            }
        }
        .sheet(isPresented: $isOnboardingShown, onDismiss: {}) {
            OnboardingView(employee: empl4, isShowing: $isOnboardingShown)
        }
    }
}
