//
//  CodeReviewView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct CodeReviewWidgetView: View {
    let viewModel: CodeReviewViewModel
    
    var body: some View {
        Image("codeReview")
            .resizable()
            .frame(width: 350, height: 220)
            .cornerRadius(12)
    }
}
