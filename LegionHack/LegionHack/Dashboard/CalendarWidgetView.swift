//
//  CalendarView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct CalendarWidgetView: View {
    let viewModel: CalendarViewModel
    
    var body: some View {
        Image("calendarWidget")
            .resizable()
            .frame(width: 350, height: 350)
            .cornerRadius(12)
    }
}
