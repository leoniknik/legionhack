//
//  LearningItemView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 03.10.2021.
//

import SwiftUI

struct LearningItemViewModel: Identifiable {
    let id = UUID()
    let title: String
    let count: String
    let imageName: String
}

class LearningListViewModel: ObservableObject {
    @Published var myCources: [LearningItemViewModel] = [
        .init(title: "Базовое администрирование Linux", count: "16", imageName: "linux"),
        .init(title: "Программирование на Python", count: "5", imageName: "python"),
        .init(title: "Алгоритмы и стуктуры данных", count: "40", imageName: "alg"),
    ]
    
    @Published var commonCources: [LearningItemViewModel] = [
        .init(title: "Использование Docker", count: "4", imageName: "docker"),
        .init(title: "Swift", count: "18", imageName: "swift"),
        .init(title: "Публичные выступления", count: "12", imageName: "talks"),
        .init(title: "Технический английский", count: "12", imageName: "eng")
    ]
}

struct LearningListView: View {
    @StateObject var viewModel = LearningListViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Мои курсы")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                myCourcesView
                Text("Доступные курсы")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                commonCourcesView
            }
        }.padding().navigationTitle("Обучение")
    }
    
    private var myCourcesView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.myCources) { group in
                LearningView(viewModel: group, needBadge: false)
            }
        }
    }
    
    private var commonCourcesView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.commonCources) { group in
                LearningView(viewModel: group, needBadge: false)
            }
        }
    }
}

struct LearningListView_Previews: PreviewProvider {
    static var previews: some View {
        LearningListView()
    }
}
