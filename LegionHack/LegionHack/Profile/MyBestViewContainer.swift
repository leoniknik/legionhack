//
//  MyBestViewContainer.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 02.10.2021.
//

import Foundation
import SwiftUI

//var body: some View {
//    MyBestViewContainer(employee: empl4)
//        .navigationTitle("Мои достижения")
//}

struct MyBestViewContainer: View {
    var employee: Employee
    var body: some View {
        VStack(alignment: .leading) {
            HStack{
                VStack(alignment: .leading) {
                    Text("Я легионер")
                        .font(.title)
                        .lineLimit(2)
                    Text("И вот чем я горжусь")
                        .font(.title2)
                        .foregroundColor(.secondary)
                }
                Spacer()
                Image(uiImage: employee.image)
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(Circle())
            }
            ForEach(employee.myBest) { model in
                MyBestElementView(model: model)
            }
            HStack() {
                Spacer()
                Image(uiImage: Asset.share.image)
                    .resizable()
                    .frame(width: 200, height: 40)
                
                Spacer()
            }
            Spacer()
        }.padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
    }
}

struct MyBestViewModel: Identifiable {
    var id = UUID()
    var title: String
    var value: Float
    var color: Color
    static var stub: MyBestViewModel {
        return MyBestViewModel(title: "stub # stub", value: 0.67, color: .red)
    }
}

struct MyBestElementView: View {
    @State var model: MyBestViewModel
    var body: some View {
        HStack(alignment: .center) {
            ProgressBar(progress: self.$model.value, color: model.color)
                .frame(width: 90, height: 90)
                .padding(10)
            Text(model.title.replacingOccurrences(of: "#", with: " \(Int(model.value * 100))% "))
                .lineLimit(3)
        }
    }
}
