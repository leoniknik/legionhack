//
//  ConferenceItemView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 03.10.2021.
//

import Foundation

import SwiftUI

struct ConferenceItemView: View {
    let viewModel: ConferencesItemViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 6) {
            ZStack(alignment: .topTrailing) {
                Image(viewModel.imageName)
                        .resizable()
                        .frame(width: 140, height: 140, alignment: .center)
                        .cornerRadius(16)
            }
            Text(viewModel.title)
                .font(.system(size: 16, weight: .bold))
                .lineLimit(2)
                .multilineTextAlignment(.center)
        }
    }
}
