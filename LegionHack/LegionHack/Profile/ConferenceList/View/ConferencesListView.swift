//
//  ConferencesListView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 03.10.2021.
//

import SwiftUI

struct ConferencesItemViewModel: Identifiable {
    let id = UUID()
    let title: String
    let imageName: String
}

class ConferencesListViewModel: ObservableObject {
    @Published var myConferences: [ConferencesItemViewModel] = [
        .init(title: "MBLT", imageName: "mblt")
    ]
    
    @Published var commonConferences: [ConferencesItemViewModel] = [
        .init(title: "Mobius", imageName: "mobius"),
        .init(title: "WWDC", imageName: "wwdc"),
        .init(title: "CocoaHeads", imageName: "cocoaheds")
    ]
}

struct ConferencesListView: View {
    @StateObject var viewModel = ConferencesListViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Участвую")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                myGroupsView
                Text("Доступные")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                commonGroupsView
            }
        }.padding().navigationTitle("Конференции")
    }
    
    private var myGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.myConferences) { group in
                ConferenceItemView(viewModel: group)
            }
        }
    }
    
    private var commonGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.commonConferences) { group in
                ConferenceItemView(viewModel: group)
            }
        }
    }
}

struct ConferencesListView_Previews: PreviewProvider {
    static var previews: some View {
        ConferencesListView()
    }
}
