//
//  ShopListView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 03.10.2021.
//

import SwiftUI

struct ShopItemViewModel: Identifiable {
    let id = UUID()
    let title: String
    let count: String
    let imageName: String
}

class ShopListViewModel: ObservableObject {
    @Published var boughtGoods: [GroupItemViewModel] = [
        .init(title: "AirPods", count: "250", imageName: "airpods"),
        .init(title: "Кружка", count: "10", imageName: "cup")
    ]
    
    @Published var availableGoods: [GroupItemViewModel] = [
        .init(title: "iPhone 13", count: "2000", imageName: "iphone"),
        .init(title: "Рюкзак", count: "50", imageName: "bag"),
        .init(title: "Компьютерное кресло", count: "200", imageName: "chair"),
        .init(title: "Зонтик", count: "30", imageName: "umbrella")
    ]
}

struct ShopListView: View {
    @StateObject var viewModel = ShopListViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Товары за бонусные баллы компании")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                myGroupsView
                Text("Доступные скидки и предложения")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                commonGroupsView
            }
        }.padding()
    }
    
    private var myGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.boughtGoods) { group in
                GroupItemView(viewModel: group)
            }
        }
    }
    
    private var commonGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.availableGoods) { group in
                GroupItemView(viewModel: group, isNeedBadge: false)
            }
        }
    }
}

struct ShopListView_Previews: PreviewProvider {
    static var previews: some View {
        ShopListView()
    }
}
