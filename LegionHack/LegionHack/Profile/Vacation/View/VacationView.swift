//
//  VacationView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct VacationView: View {
    @State var text: String = ""
        
        var body: some View {
            VStack {
                Text("У вас осталось 19 дней отпуска")
                List {
                    Text("3.06.2021 - 18.06.2021")
                    Text("1.09.2021 - 5.09.2021")
                    Section(footer: confirmButton) {
                        NavigationLink(destination: Group {}) {
                                Text("Выбор дат")
                            }
                        }
                }.navigationTitle("Отпуска")
            }
        }
        
        var confirmButton: some View {
            Button {
               print("")
            } label: {
                HStack {
                    Spacer()
                    Text("Оформить новый отпуск")
                        .foregroundColor(.white)
                        .font(.system(size: 18))
                    Spacer()
                }
            }.padding(8)
            .background(legionBlue)
            .cornerRadius(8)
        }
}

struct VacationView_Previews: PreviewProvider {
    static var previews: some View {
        VacationView()
    }
}
