//
//  QuestView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct QuestView: View {
    var body: some View {
        QuestListView()
    }
}

struct QuestView_Previews: PreviewProvider {
    static var previews: some View {
        QuestView()
    }
}
