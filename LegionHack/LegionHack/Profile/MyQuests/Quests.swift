//
//  Quests.swift
//  LegionHack
//
//  Created by Evtukhov Aleksander on 03.10.2021.
//

import Foundation
import SwiftUI

struct G1roupItemViewModel: Identifiable {
    let id = UUID()
    let title: String
    let count: String
    let imageName: String
}

struct SingleQuestViewModel: Identifiable {
    var id = UUID()
    var star: Int
    var type: String
    var title: String
    var price: Int
    var todo: String
    var empl: Employee
    var progress: Float
    var color: Color
    var days: Int
}

struct SingleQuestView: View {
    var singleQuestViewModel: SingleQuestViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            VStack(alignment: .leading) {
                HStack() {
                    starView
                    Rectangle().frame(width: 10, height: 0)
                    Text(singleQuestViewModel.type)
                        .foregroundColor(.white)
                }
                Divider()
                
                Text(singleQuestViewModel.title)
                    .font(.title)
                    .foregroundColor(.white)
                
                HStack() {
                    Text("\(singleQuestViewModel.price) talant points").foregroundColor(legionLightBlue).padding(EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)).addBorder(legionBlue, width: 3, cornerRadius: 5)
                    Text("за задание").foregroundColor(.gray)
                }
                Divider()
                Text(singleQuestViewModel.todo).foregroundColor(legionLightBlue)
            }.padding(EdgeInsets(top: 16, leading: 20, bottom: 0, trailing: 20))
            
            HStack {
                EmployeesView(
                    employeesViewModel: EmployeesViewModel(title: "Автор задания", employees: [singleQuestViewModel.empl]),
                    hideIco: true
                    //background: legionLightBlue
                )
                    .frame(width: 200, height: 200)
                    .padding(EdgeInsets(top: 0, leading: 16, bottom: 5, trailing: 16))
                    
                Spacer()
                RestView(
                    progress: singleQuestViewModel.progress,
                    color: singleQuestViewModel.color,
                  //  background: legionLightBlue,
                    days: singleQuestViewModel.days
                )
                    .frame(width: 120, height: 200)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 16))
            
            }
            
            Button(action: { }) {
                HStack {
                    Spacer()
                    Text("Взять")
                        .foregroundColor(legionBlack)
                    Spacer()
                }
               }
                .frame(height: 40)
                .background(Color.white)
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .padding()
            
            
        }
        .background(legionBlack)
        .clipShape(RoundedRectangle(cornerRadius: 10))
    }
    
    @ViewBuilder
    var starView: some View {
        switch singleQuestViewModel.star {
        case 1:
            HStack() {
                Image(systemName: "star.fill").foregroundColor(.white)
                Image(systemName: "star")
                Image(systemName: "star")
                Image(systemName: "star")
                Image(systemName: "star")
            }.foregroundColor(.white)
        case 2:
            HStack() {
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star")
                Image(systemName: "star")
                Image(systemName: "star")
            }.foregroundColor(.white)
        case 3:
            HStack() {
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star")
                Image(systemName: "star")
            }.foregroundColor(.white)
        case 4:
            HStack() {
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star")
            }.foregroundColor(.white)
        case 5:
            HStack() {
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
            }.foregroundColor(.white)
        default:
            Text("Stub")
        }
        
        
    }
}

extension View {
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        let roundedRect = RoundedRectangle(cornerRadius: cornerRadius)
        return clipShape(roundedRect)
            .overlay(roundedRect.strokeBorder(content, lineWidth: width))
    }
}

struct QuestListViewModel {
    
}

struct QuestListView: View {
    @State var viewModel = QuestListViewModel()
    @State var favoriteColor: Int = 0
    var elements:  [SingleQuestViewModel] {
        favoriteColor == 0 ? quests : quests2
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text("Квесты сообщества")
                    .font(.system(size: 20, weight: .bold))
                Text("Выполняй инетресные задачи и зарабатывай очки талантов (talent points)")
                    .font(.system(size: 15, weight: .regular))
                Divider()
                Picker("", selection: $favoriteColor) {
                    Text("Мои активные квесты").tag(0)
                    Text("Доступные для выполнения квесты").tag(1)
                }
                .pickerStyle(.segmented)
                Divider()
                ForEach(elements) { quest in
                    SingleQuestView(singleQuestViewModel: quest)
                }
            }
        }.padding()
    }
}

struct G1roupListView_Previews: PreviewProvider {
    static var previews: some View {
        GroupListView()
    }
}
