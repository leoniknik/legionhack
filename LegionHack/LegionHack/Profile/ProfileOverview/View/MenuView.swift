//
//  MenuView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

enum MenuItemViewModel: Identifiable, CaseIterable {
    var id: String { title }
    
    case achievements
    case health
    case vacation
    case conference
    case department
    case referenceList
    case shop
    case info
    case quest
    case learning
    case competence
    case quit
    case teamBuilding
    case assessment
    case calendar
    
    var title: String {
        switch self {
        case .achievements:
            return "Достижения"
        case .health:
            return "Здоровье"
        case .vacation:
            return "Отпуск"
        case .conference:
            return "Конференции"
        case .department:
            return "Мой отдел"
        case .referenceList:
            return "Справки"
        case .shop:
            return "Магазин"
        case .info:
            return "Информация о компании"
        case .quest:
            return "Квесты"
        case .learning:
            return "Обучение"
        case .competence:
            return "Карта компетенций"
        case .quit:
            return "Увольнение"
        case .teamBuilding:
            return "Тимбилдинги"
        case .assessment:
            return "Оценочные мероприятия"
        case .calendar:
            return "Календарь"
        }
    }
    
    var imageName: String {
        switch self {
        case .achievements:
            return "10.square.fill"
        case .health:
            return "cross.case.fill"
        case .vacation:
            return "sun.haze.fill"
        case .conference:
            return "brain"
        case .department:
            return "person.3"
        case .referenceList:
            return "list.bullet.rectangle.fill"
        case .shop:
            return "cart"
        case .info:
            return "info.circle"
        case .quest:
            return "gamecontroller.fill"
        case .learning:
            return "book"
        case .competence:
            return "graduationcap"
        case .quit:
            return "x.circle"
        case .teamBuilding:
            return "latch.2.case.fill"
        case .assessment:
            return "checkmark.diamond.fill"
        case .calendar:
            return "calendar"
        }
    }
}

class MenuViewViewModel: ObservableObject {
    @Published var menuItems: [MenuItemViewModel] = MenuItemViewModel.allCases
}

// bip: скидки и предложения

struct MenuView: View {
    @StateObject var viewModel = MenuViewViewModel()
    let destinationFactory = MenuDestinationFactory()

    var body: some View {
        List {
            Section(header: EmployeeHeaderView(isPersonalScreen: true, employee: empl4)
                .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 16, trailing: 0))
            ) {
                ForEach(viewModel.menuItems) { item in
                    NavigationLink(destination: NavigationLazyView(destinationFactory.makeDestination(item: item))) {
                        MenuItemView(viewModel: .init(imageName: item.imageName, title: item.title))
                    }
                }
            }
        }.listStyle(GroupedListStyle())
    }
}

struct MenuDestinationFactory {
    @ViewBuilder
    func makeDestination(item: MenuItemViewModel) -> some View {
        switch item {
        case .achievements:
            MyBestViewContainer(employee: empl4)
        case .health:
            HealthView()
        case .vacation:
            VacationView()
        case .conference:
            ConferencesListView()
        case .department:
            DepartmentView()
        case .referenceList:
            ReferenceListView()
        case .shop:
            ShopListView()
        case .info:
            InfoView()
        case .quest:
            QuestView()
        case .learning:
            LearningListView()
        case .competence:
            CompetenceView()
        case .quit:
            QuestView()
        case .teamBuilding:
            TeamBuildingView()
        case .assessment:
            AssessmentView()
        case .calendar:
            CalendarView()
        }
    }
}
