//
//  MenuItemView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct MenuItemView: View {
    let viewModel: ViewModel
    
    var body: some View {
        HStack {
            Image(systemName: viewModel.imageName)
                .frame(minWidth: 40)
                .foregroundStyle(.blue)
            Text(viewModel.title)
        }
    }
}
 
extension MenuItemView {
    struct ViewModel {
        let imageName: String
        let title: String
    }
}
