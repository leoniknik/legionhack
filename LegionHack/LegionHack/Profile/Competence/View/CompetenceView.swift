//
//  CompetenceView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct CompetenceView: View {
    var mesh = Mesh.sampleProceduralMesh()
    var selection = SelectionHandler()
    @State private var selectedTab: Int = 0
    
    var body: some View {
        VStack {
            Picker("", selection: $selectedTab) {
                Text("Карта навыков").tag(0)
                Text("Дерево навыков").tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())

            switch(selectedTab) {
            case 0:
                SurfaceView(mesh: mesh, selection: selection)
            case 1:
                Image("skilltree").resizable().frame(
                    width: UIScreen.main.bounds.width - 50,
                    height: UIScreen.main.bounds.height - 200
                )
            default:
                Group {}
            }
        }
    }
}

