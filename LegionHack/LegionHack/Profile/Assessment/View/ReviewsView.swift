//
//  ReviewsView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 02.10.2021.
//

import SwiftUI

struct Review: Identifiable {
    var id: Int
    let isEditable: Bool
    let colleagueId: Colleague.ID
    let text: String
}

var reviews: [Review] = [
    .init(id: 0, isEditable: true, colleagueId: 0, text: "Очень хорошо"),
    .init(id: 1, isEditable: true, colleagueId: 1, text: "Продолжай в том же духе"),
    .init(id: 2, isEditable: false, colleagueId: 2, text: "Стоит поработать над софт скиллами")
]

struct ReviewsView: View {
    var body: some View {
        List {
            ForEach(activeResults, id: \.id) { review in
                TaskRow(review: review)
            }
        }
    }
    
    var activeResults: [Review] {
        return reviews.filter { $0.isEditable }
    }
    var arhiveResults: [Review] {
        return reviews.filter { !$0.isEditable }
    }
}

struct TaskRow: View {
    let review: Review
    
    var body: some View {
        HStack {
            Image(colleagues[review.colleagueId].imageName)
                .resizable()
                .clipShape(Circle())
                .frame(width: 50, height: 50)
                .clipped()
            VStack(alignment: .leading) {
                Text(colleagues[review.colleagueId].name).font(.headline)
                Text(review.text).font(.subheadline)
            }.padding(.leading, 8)
            if review.isEditable {
                Spacer()
                Image(systemName: "square.and.pencil")
            }
        }
    }
}
