//
//  AssessmentDetailView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 03.10.2021.
//

import SwiftUI

struct AssessmentDetailView: View {
    @State var text: String = ""
    
    var body: some View {
        List {
            Section(footer: confirmButton) {
                NavigationLink(destination: ConnectionsView()) {
                    Text("Выбор коллег")
                }
            }
        }.navigationTitle("Performance Review")
    }
    
    var confirmButton: some View {
        Button {
           print("")
        } label: {
            HStack {
                Spacer()
                Text("Запустить процесс Performance Review")
                    .foregroundColor(.white)
                    .font(.system(size: 18))
                Spacer()
            }
        }.padding(8)
        .background(legionBlue)
        .cornerRadius(8)
    }
}
