//
//  ConnectionsView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 02.10.2021.
//

import SwiftUI

struct Colleague: Identifiable {
    var id: Int

    let name: String
    let imageName: String
    let position: String
}

let colleagues: [Colleague] = [
    .init(id: 0, name: "Odin", imageName: "odin", position: "Президент"),
    .init(id: 1, name: "Tor", imageName: "tor", position: "Вице-президент"),
    .init(id: 2, name: "Hela", imageName: "hela", position: "Глава отдела кадров"),
    .init(id: 3, name: "Loki", imageName: "loki", position: "Специалист по работе с общественностью")
]


struct ConnectionsView: View {

    @State private var searchText = ""

    var body: some View {
        VStack {
            List {
                ForEach(searchResults, id: \.id) { colleague in
                    ColleagueRow(colleague: colleague)
                }
            }
        }
        .searchable(text: $searchText, prompt: "Найдите своих коллег")
        .navigationTitle("С кем вы работали?")
    }

        var searchResults: [Colleague] {
            if searchText.isEmpty || searchText == "" {
                return colleagues
            } else {
                return colleagues.filter { $0.name.contains(searchText) }
            }
        }
}


struct ColleagueRow: View {
    let colleague: Colleague

    var body: some View {
        HStack {
            Image(colleague.imageName)
                .resizable()
                .clipShape(Circle())
                .frame(width: 50, height: 50)
                .clipped()
            VStack(alignment: .leading) {
                Text(colleague.name).font(.headline)
                Text(colleague.position).font(.subheadline)
            }.padding(.leading, 8)
        }
    }
}

struct ConnectionsView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ConnectionsView()
        }
    }
}
