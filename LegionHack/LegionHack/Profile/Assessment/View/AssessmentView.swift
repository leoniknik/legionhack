//
//  AssessmentView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct AssessmentView: View {
    @State private var selectedTab: Int = 0
        
    var body: some View {
        VStack {
            Picker("", selection: $selectedTab) {
                Text("Мои оценивания").tag(0)
                Text("Оценивания коллег").tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())

            switch(selectedTab) {
            case 0:
                myAssessments
            case 1:
                otherAssessments
            default:
                Group {}
            }
        }
    }
    
    private var myAssessments: some View {
        List {
            Section(header: Text("Ближайшие оценочные мероприятия").foregroundColor(legionBlack)) {
                NavigationLink(destination: AssessmentDetailView()) {
                    AssessmentRowView(title: "Performance Review")
                }
                NavigationLink(destination: Text("") ) {
                    AssessmentRowView(title: "One on One с функциональным руководителем")
                }
            }

            Section(header: Text("Прошедшие оценочные мероприятия").foregroundColor(legionBlack)) {
                AssessmentRowView(title: "Welcome встреча с руководителем")
                AssessmentRowView(title: "Welcome встреча с HR")
                AssessmentRowView(title: "Входное интервью")
            }
        }
    }
    
    private var otherAssessments: some View {
        ReviewsView()
    }
}

struct AssessmentRowView: View {
    let title: String
    
    var body: some View {
        Text(title)
    }
}
