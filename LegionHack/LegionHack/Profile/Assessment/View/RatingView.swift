//
//  RatingView.swift
//  LegionHack
//
//  Created by Roman Nordshtrem on 02.10.2021.
//

import SwiftUI

struct Rating: Identifiable {
    var id: Int

    var count: Int
    var text: String
}

var ratings: [Rating] = [
    .init(id: 0, count: 1, text: "Выше ожидаемого"),
    .init(id: 1, count: -1, text: "Что-то не получилось")
]

struct RatingView: View {

    @State var panel = 0
    var panels = ["Текущая", "Архив"]
    
    var body: some View {
        NavigationView {
            VStack {
                Picker("Tasks", selection: $panel) {
                    ForEach(0 ..< panels.count) { index in
                        Text(self.panels[index])
                            .tag(index)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.top, 16)
                Spacer()
                List {
                    if panel == 0 {
                        ForEach(activeResults, id: \.id) { rating in
                            RatingRow(rating: rating)
                        }
                    } else {
                        ForEach(arhiveResults, id: \.id) { rating in
                            RatingRow(rating: rating)
                        }
                    }
                }
                .navigationBarTitle("Мои оценки")
                
            }
        }
    }
    
    var activeResults: [Rating] {
        return [ratings[0]]
    }
    var arhiveResults: [Rating] {
        return [ratings[1]]
    }
}

struct RatingRow: View {
    let rating: Rating
    
    var body: some View {
        HStack {
            Text(rating.text).font(.subheadline)
            Spacer()
            Text(String(rating.count)).font(.headline)
                .padding(.trailing, 16)
        }
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        RatingView()
    }
}
