

import Foundation
import CoreGraphics

extension Mesh {
  func add(text: String, rt: Node) -> Node {
    let distance = CGFloat(200)
    let pos = positionForNewChild(rt, length: distance)
    let node = self.addChild(rt, at: pos)
    self.updateNodeText(node, string: text)
    
    return node
  }
  
  func add(text: String, index: Int, rt: Node) -> Node {

    let point = self.pointWithCenter(center: .zero, radius: 200, angle: (index * 90 + 30).radians)
    let node = self.addChild(rt, at: point)
    self.updateNodeText(node, string: text)
    
    return node
  }

  static func sampleProceduralMesh() -> Mesh {
    let mesh = Mesh()
        
    let a = mesh.add(text: "Архитектура", index: 0, rt: mesh.rootNode())
      let a1 = mesh.add(text: "Простые", rt: a)
        let a11 = mesh.add(text: "MVC", rt: a1)
        let a12 = mesh.add(text: "MVP", rt: a1)
      let a2 = mesh.add(text: "Сложные", rt: a)
       let a21 = mesh.add(text: "Реактивные", rt: a2)
            let a212 = mesh.add(text: "MVVM", rt: a21)
       let a22 = mesh.add(text: "Нереактивные", rt: a2)
           let a221 = mesh.add(text: "VIPER", rt: a22)
           let a222 = mesh.add(text: "VIP циклы", rt: a22)
      
    
//    let a = mesh.add(text: "Android", index: 2, rt: mesh.rootNode())
    let c = mesh.add(text: "Многопоточность", index: 3, rt: mesh.rootNode())
    let с1 = mesh.add(text: "Swift-Concurency", rt: c)
      let c11 = mesh.add(text: "async await", rt: с1)
      let c12 = mesh.add(text: "Actors", rt: с1)
      let c13 = mesh.add(text: "Типы", rt: с1)
           let c131 = mesh.add(text: "Structured", rt: c13)
           let c132 = mesh.add(text: "UnStructured", rt: c13)
            let c133 = mesh.add(text: "Deatached", rt: c13)
    let c2 = mesh.add(text: "GCD", rt: c)
    
    
      
      let i = mesh.add(text: "Swift", index: 1, rt: mesh.rootNode())
      let i1 = mesh.add(text: "Optionals", rt: i)
      let i2 = mesh.add(text: "enums", rt: i)
      let i3 = mesh.add(text: "structs", rt: i)
      let i4 = mesh.add(text: "classes", rt: i)
      
      
      let z = mesh.add(text: "Compilation", index: 2, rt: mesh.rootNode())
      let z1 = mesh.add(text: "xcodebuild", rt: z)
      let z2 = mesh.add(text: "dylib", rt: z)
      let z3 = mesh.add(text: "xcframeworks", rt: z)
      let z4 = mesh.add(text: "classes", rt: z)
    
    
    return mesh
  }

  func addChildrenRecursive(to node: Node, distance: CGFloat, generation: Int) {
    let labels = ["A", "B", "C", "D", "E", "F"]
    guard generation < labels.count else {
      return
    }

    let childCount = Int.random(in: 1..<4)
    var count = 0
    while count < childCount {
      count += 1
      let position = positionForNewChild(node, length: distance)
      let child = addChild(node, at: position)
      updateNodeText(child, string: "\(labels[generation])\(count + 1)")
      addChildrenRecursive(to: child, distance: distance + 200.0, generation: generation + 1)
    }
  }
}

extension Int {
  var radians: CGFloat {
    CGFloat(self) * CGFloat.pi/180.0
  }
}
