//
//  BotView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

let botChatDialog = ChatDetailViewModel(messages: [
    .init(
        isMy: false,
        text: "Привет! Меня зовут Энерджайзер, я буду твоим лучшим другом и помощником все то время, пока ты часть прекрасной команды легионеров. Пиши мне по любому поводу - я постараюсь помочь или подсказать ;)",
        avatarName: "bot",
        senderName: "Бот"
    ),
    .init(
        isMy: true,
        text: "Круто! Спасибо)",
        avatarName: "",
        senderName: ""
    ),
    .init(
        isMy: false,
        text: "А еще я постараюсь тебе напоминать о различных важных событиях - такие как дни рождения твоих коллег, скорый перфоманс ревью или же ты просто можешь попросить меня рассказать тебе новости в компании",
        avatarName: "bot",
        senderName: "Бот"
    ),
    .init(
        isMy: false,
        text: "Ах да, чуть не забыл, вечно барахлит мой CPU, ознакомься, пожалуйста, с Welcome Book по данной ссылке - https://drive.google.com/file/d/1bgoPkv0efyO59DWQr4DbwbYYw_XzXVhx/view?usp=sharing",
        avatarName: "bot",
        senderName: "Бот",
        deepLink: URL(string: "https://drive.google.com/file/d/1bgoPkv0efyO59DWQr4DbwbYYw_XzXVhx/view?usp=sharing")
    ),
    .init(
        isMy: true,
        text: "Расскажи мне, где мне прочитать про мой текущий проект",
        avatarName: "",
        senderName: ""
    ),
    .init(
        isMy: false,
        text: "Нажми на это сообщение, чтобы узнать все о своем текущем проекте - iOS Awesome App",
        avatarName: "bot",
        senderName: "Бот"
    ),
    .init(
        isMy: false,
        text: "Ку-ку, база данных нашептала мне, что сегодня День Рождения Никиты Жеребцова - не забудь его поздравить)",
        avatarName: "bot",
        senderName: "Бот"
    ),
])

struct BotView: View {
    var body: some View {
        VStack {
            ChatDetailView(viewModel: botChatDialog)
            messageInputView.padding()
        }
    }
    
    private var messageInputView: some View {
        MessageInputView()
    }
}

struct MessageInputView: View {
    @State private var text: String = ""
    
    var body: some View {
        HStack(alignment: .center, spacing: 8) {
            TextField("Введите сообщение", text: $text)
                .padding()
                .background(.regularMaterial)
                .cornerRadius(6)
            Button {
                print("")
            } label: {
                Image(systemName: "paperplane.circle")
                    .resizable()
                    .frame(width: 40, height: 40)
            }
        }
    }
}
