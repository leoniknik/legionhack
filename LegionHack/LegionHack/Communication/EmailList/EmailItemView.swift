//
//  EmailItemView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct EmailItemView: View {
    let viewModel: EmailItemViewModel
    
    var body: some View {
        
        HStack(alignment: .top, spacing: 8) {
            VStack(alignment: .leading, spacing: 4) {
                Text(viewModel.from)
                    .lineLimit(1)
                    .font(.system(size: 14, weight: .semibold))
                Text(viewModel.title)
                    .lineLimit(1)
                    .font(.system(size: 14, weight: .semibold))
                Text(viewModel.message)
                    .lineLimit(2)
                    .font(.system(size: 12))
                    .foregroundColor(.gray)
            }
            Spacer()
            Text(viewModel.dateString)
                .font(.system(size: 14))
                .foregroundColor(.gray)
        }.padding()
    }
}
