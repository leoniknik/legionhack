//
//  EmailListView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct EmailItemViewModel: Identifiable {
    var id = UUID()
    let title: String
    let message: String
    let from: String
    let dateString: String
}

class EmailListViewModel: ObservableObject {
    @Published var emails: [EmailItemViewModel] = [
        .init(
            title: "Обсуждение проекта",
            message: "Напоминаю, что сегодня в 16:00 обсуждение деталей нового проекта",
            from: "Михаил Баранников (Руководиль мобильной разработки)",
            dateString: "12:30"
        ),
        .init(
            title: "Результаты ретро",
            message: "Ребят, большое спасибо, что поучаствовали в ретро, было очень продуктивно, результаты прикладываю в почте",
            from: "Миша (PM)",
            dateString: "Вчера"
        ),
        .init(
            title: "Invitation accepted",
            message: "nord.r@inbox.ru, now known as Roman, has accepted your invitation to join the Kirill Volodin / LegionHack project.",
            from: "GitLab",
            dateString: "Вчера"
        )
    ]
}

struct EmailListView: View {
    @StateObject var viewModel = EmailListViewModel()
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: [GridItem(.flexible())], spacing: .zero) {
                ForEach(viewModel.emails) { email in
                    EmailItemView(viewModel: email)
                }
            }
        }
    }
}
