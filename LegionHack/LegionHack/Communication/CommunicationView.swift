//
//  CommunicationView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

struct CommunicationView: View {
    @State private var selectedTab: Int = 0
    
    var body: some View {
        VStack {
            Picker("", selection: $selectedTab) {
                Text("Чаты").tag(0)
                Text("Почта").tag(1)
                Text("Сообщества").tag(2)
                Text("Бот").tag(3)
            }
            .pickerStyle(SegmentedPickerStyle())

            switch(selectedTab) {
            case 0:
                Group {
                    ChatListView()
                    Spacer()
                }
            case 1:
                Group {
                    EmailListView()
                    Spacer()
                }
            case 2:
                Group {
                    GroupListView()
                    Spacer()
                }
            case 3:
                Group {
                    BotView()
                    Spacer()
                }
            default:
                fatalError()
            }
        }
    }
}
