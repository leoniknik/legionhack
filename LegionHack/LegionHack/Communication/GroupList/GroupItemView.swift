//
//  GroupItemView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct GroupItemView: View {
    let viewModel: GroupItemViewModel
    var isNeedBadge = true
    
    var body: some View {
        VStack(alignment: .center, spacing: 6) {
            ZStack(alignment: .topTrailing) {
                Image(viewModel.imageName)
                        .resizable()
                        .frame(width: 140, height: 140, alignment: .center)
                        .cornerRadius(16)
                if isNeedBadge {
                    countView
                }
            }
            Text(viewModel.title)
                .font(.system(size: 16, weight: .bold))
                .lineLimit(2)
                .multilineTextAlignment(.center)
        }
    }
    
    private var countView: some View {
        ZStack {
            Circle()
                .fill(.blue)
                .frame(width: 30, height: 30)
            Text(viewModel.count)
                .foregroundColor(.white)
                .font(.system(size: 16, weight: .bold))
        }
    }
}
