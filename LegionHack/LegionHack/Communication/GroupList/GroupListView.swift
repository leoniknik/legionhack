//
//  GroupListView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct GroupItemViewModel: Identifiable {
    let id = UUID()
    let title: String
    let count: String
    let imageName: String
}

class GroupListViewModel: ObservableObject {
    @Published var myGroups: [GroupItemViewModel] = [
        .init(title: "Йога в офисе", count: "16", imageName: "yoga"),
        .init(title: "Шахматный клуб", count: "5", imageName: "chess"),
        .init(title: "English Club", count: "40", imageName: "english"),
    ]
    
    @Published var commonGroups: [GroupItemViewModel] = [
        .init(title: "Любители ракет и сладких конфет", count: "4", imageName: "neitrino"),
        .init(title: "Компьютерные игры", count: "18", imageName: "computerGames"),
        .init(title: "Клуб настольных игр", count: "12", imageName: "games"),
        .init(title: "Худеем к лету", count: "12", imageName: "sport")
    ]
}

struct GroupListView: View {
    @StateObject var viewModel = GroupListViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Мои сообщества")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                myGroupsView
                Text("Доступные сообщества")
                    .font(.system(size: 20, weight: .bold))
                    .padding()
                commonGroupsView
            }
        }.padding()
    }
    
    private var myGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.myGroups) { group in
                GroupItemView(viewModel: group)
            }
        }
    }
    
    private var commonGroupsView: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.commonGroups) { group in
                GroupItemView(viewModel: group)
            }
        }
    }
}

struct GroupListView_Previews: PreviewProvider {
    static var previews: some View {
        GroupListView()
    }
}
