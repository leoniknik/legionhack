//
//  ChatDetailView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct MessageViewModel: Identifiable {
    let id = UUID()
    let isMy: Bool
    let text: String
    let avatarName: String
    let senderName: String
    var deepLink: URL?
}

class ChatDetailViewModel: ObservableObject {
    @Published var messages: [MessageViewModel]
    
    init(messages: [MessageViewModel]) {
        self.messages = messages
    }
}

struct ChatDetailView: View {
    @ObservedObject var viewModel: ChatDetailViewModel
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: [GridItem(.flexible())], spacing: 14) {
                ForEach(viewModel.messages) { message in
                    ChatMessageView(viewModel: message).padding([.leading, .trailing], 16)
                }
            }
        }
    }
}

struct ChatMessageView: View {
    let viewModel: MessageViewModel
    
    var body: some View {
        if viewModel.isMy {
            myMessageView
        } else {
            messageView
        }
    }
    
    private var myMessageView: some View {
        HStack {
            Spacer()
            messageTextView
        }
    }
    
    private var messageView: some View {
        HStack(alignment: .center, spacing: 16) {
            VStack {
                avatarView
                senderNameView
            }
            messageTextView
            Spacer()
        }
    }
    
    private var senderNameView: some View {
        Text(viewModel.senderName)
            .font(.system(size: 12))
    }
    
    private var avatarView: some View {
        Image(viewModel.avatarName)
            .resizable()
            .frame(width: 50, height: 50)
            .cornerRadius(25)
    }
    
    private var messageTextView: some View {
        Text(viewModel.text)
            .font(.system(size: 16))
            .foregroundColor(.white)
            .padding()
            .background(viewModel.isMy ? legionBlue : legionBlack)
            .cornerRadius(8)
            .onTapGesture {
                guard let url = viewModel.deepLink, UIApplication.shared.canOpenURL(url) else {
                    return
                }
                UIApplication.shared.open(url)
            }
    }
}

let legionBlue = Color(.sRGB, red: 0, green: 174 / 255.0, blue: 239 / 255.0, opacity: 1.0)
let legionBlack = Color(.sRGB, red: 28 / 255, green: 39 / 255.0, blue: 58 / 255.0, opacity: 1.0)
let legionLightBlue = Color(.sRGB, red: 232 / 255, green: 247 / 255.0, blue: 254 / 255.0, opacity: 1.0)
