//
//  ImportantChatView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct ImportantChatItemView: View {
    let viewModel: ImportantContact
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Image(viewModel.avatarName)
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .cornerRadius(30)
            Text(viewModel.title)
        }
    }
}
