//
//  ChatItemView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct ChatItemView: View {
    let viewModel: ChatViewModel
    
    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            Image(viewModel.avatarName)
                .resizable()
                .frame(width: 60, height: 60)
                .cornerRadius(30)
            VStack(alignment: .leading, spacing: 6) {
                Text(viewModel.title)
                    .lineLimit(1)
                    .font(.system(size: 18, weight: .bold))
                Text(viewModel.chatPreviewMessage)
                    .lineLimit(2)
                    .font(.system(size: 14, weight: .regular))
                    .foregroundColor(.gray)
            }
            Spacer()
            Text(viewModel.dateString)
                .foregroundColor(.gray)
                .font(.system(size: 12, weight: .regular))
        }.padding()
    }
}
