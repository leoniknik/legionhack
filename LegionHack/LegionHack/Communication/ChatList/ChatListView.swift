//
//  ChatListView.swift
//  LegionHack
//
//  Created by Кирилл Володин on 02.10.2021.
//

import SwiftUI

struct ChatViewModel: Identifiable {
    var id: String { title }
    let title: String
    let lastMessage: String
    let lastMessagefrom: Employee
    let avatarName: String
    let dateString: String
    let isGroup: Bool
    
    var chatPreviewMessage: String {
        if isGroup {
            return "\(lastMessagefrom.name): \(lastMessage)"
        } else {
           return lastMessage
        }
    }
}

struct ImportantContact: Identifiable {
    var id: String { title }
    let title: String
    let avatarName: String
}

class ChatListViewModel: ObservableObject {
    var importantContacts: [ImportantContact] = [
        .init(title: "HR", avatarName: "hrChat"),
        .init(title: "Ментор", avatarName: "mentor"),
        .init(title: "Босс", avatarName: "boss")
    ]
    
    var chatModel = ChatDetailViewModel(messages: [
        .init(isMy: false, text: "Добро пожаловать в семью", avatarName: "mentor", senderName: "Ментор", deepLink: nil)
    ])
    
    var commonChats: [ChatViewModel] = [
        .init(
            title: "iOS Отдел",
            lastMessage: "Ребят кто может рассказать, какие подводные камни при переходе на SPM",
            lastMessagefrom: empl1,
            avatarName: "xcode",
            dateString: "15:26",
            isGroup: true
        ),
        .init(
            title: "Паша (iOS)",
            lastMessage: "Привет, можешь мне помочь поправить один баг",
            lastMessagefrom: empl2,
            avatarName: "man1",
            dateString: "10:19",
            isGroup: false
        ),
        .init(
            title: "Леша (Дизайнер)",
            lastMessage: "Я поправил макет в Figma, можешь посмотреть",
            lastMessagefrom: empl2,
            avatarName: "man2",
            dateString: "Вчера",
            isGroup: false
        ),
        .init(
            title: "Миша (PM)",
            lastMessage: "Как закончишь - напиши, подумаем, что дальше делать",
            lastMessagefrom: empl2,
            avatarName: "man3",
            dateString: "Вчера",
            isGroup: false
        )
    ]
}

struct ChatListView: View {
    @StateObject var viewModel = ChatListViewModel()
    
    var body: some View {
        VStack {
            importantChatsList.padding(EdgeInsets(top: .zero, leading: 16, bottom: .zero, trailing: 16))
            chatList
            Spacer()
        }
    }
    
    private var importantChatsList: some View {
        ScrollView(.horizontal) {
            LazyHGrid(rows: [GridItem(.flexible())], spacing: 30) {
                ForEach(viewModel.importantContacts) { chat in
                    if chat.title == "Ментор" {
                        NavigationLink(destination: VStack {
                            ChatDetailView(viewModel: viewModel.chatModel)
                            MessageInputView()
                        }.padding()) {
                            ImportantChatItemView(viewModel: chat)
                        }
                    } else {
                        ImportantChatItemView(viewModel: chat)
                    }
                }
            }
        }.frame(height: 100)
    }
    
    private var chatList: some View {
        LazyVGrid(columns: [GridItem(.flexible())], spacing: 10) {
            ForEach(viewModel.commonChats) { chat in
                ChatItemView(viewModel: chat)
            }
        }
    }
}
