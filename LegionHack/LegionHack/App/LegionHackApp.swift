//
//  LegionHackApp.swift
//  LegionHack
//
//  Created by Кирилл Володин on 01.10.2021.
//

import SwiftUI

@main
struct LegionHackApp: App {
    var body: some Scene {
        WindowGroup {
            AuthTabBar()
        }
    }
}

